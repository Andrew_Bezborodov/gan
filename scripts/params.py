
#________________________Description______________________#
"""
	meanNoize - the mean of noize, thats give to generator
	stdNoize - the mean of noize, thats give to generator
	lenNoize - the length of sound, that give to generator
	dropout - the value of dropout in generator
	numMusicType - the number of music, that uses in GAN
	epoch - number of epochs
	butchSize - the number of batch size
	epochGen - the number of epochs of Generator
	epochDes - the number of epochs of Descriminator
	normalOfSongs - the type of normalisation. Someone of
	["normal"]
"""
#________________________Description______________________#

params = {
	"meanNoize":0,
	"stdNoize":1,
	"lenNoize":256,
	"dropout":0.2,
	"numMusicType":8,
	"epoch":3000,
	"epochGen":5,
	"epochDes":5,
	"normalOfSongs":"normal",
    "lr":0.02,
    "beta1":0.9,
    "train":0.7,
    "equelSongsInBatch":20,
    "songsInBatch":10
}

params["hiddenSizeGener"] = 1
params["butchSize"] = params["equelSongsInBatch"] * params["songsInBatch"]
