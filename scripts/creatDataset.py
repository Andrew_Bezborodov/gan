
# coding: utf-8

# In[1]:


import models.models_structure as ms
from params import params
import torch
import pandas as pd
from settings import settings
import numpy as np
from scipy.io import wavfile


# In[2]:


## Make target


# In[3]:


def readData(settings):
    data = pd.read_csv(settings["fileNameData"])
    data["train"] = np.random.random(len(data)) < params["train"]
    return data


def uniqueTypeMusic(data):
    musicTypes = data["type"].unique()

    d = []
    for i,value in enumerate(musicTypes):
        k = [0] * len(musicTypes)
        k[i] = 1
        d.append((value,k))

    return dict(d)
    


# In[15]:


readData(settings).drop_duplicates()


# In[45]:


## Subdivide to train and test
def divideData(data):
    train = data[data["train"]]
    train.index = range(len(train))
    test = data[data["train"] != True]
    test.index = range(len(test))
    return train, test



# In[46]:



def createOneButch( tempName,settings,params,d):
    bigDataFitur = []
    bigDataTarget = []

    for i in tempName.index:
        try:
            fs, data = wavfile.read(settings['folderData'] + tempName.loc[i]["addres"])
            print(tempName.loc[i]["addres"])
        except:
            print("Address ",tempName.loc[i]["addres"])
        fitur = []
        target = []

        for _ in range(params["equelSongsInBatch"]):
            start = np.random.randint(len(data.T[0]) - params["lenNoize"])
            end = start + params["lenNoize"]


            fitur.append(data.T[0][start:end]),

            target.append(d[tempName.loc[i]["type"]])

        bigDataFitur += fitur
        bigDataTarget += target

    ## fitur - real data for descreminator
    ## target - noize for generator

    fitur = torch.Tensor(bigDataFitur).view(params["butchSize"],params["lenNoize"],1)
    target = torch.Tensor(bigDataTarget).view(params["butchSize"],1,params["numMusicType"])
    return fitur, target
 
def makeIndex(train, params):
    while True:
        p = train.loc[np.random.choice(len(train),params['songsInBatch'])].index
        if len(p.unique()) == len(p):
            break
    return p


data = readData(settings)
d = uniqueTypeMusic(data)
train, test = divideData(data)


p = makeIndex(train, params)
tempName = train.loc[p]
fitur, target = createOneButch( tempName,settings,params,d)


# In[16]:


netD = ms.Descreminator(params)
print("Output Desc:",netD(fitur).shape)

netG = ms.Generator(params)
print("Output Gene:",netG(target).shape)


# In[9]:




