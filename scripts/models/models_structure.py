import torch
import torch.nn as nn


class Descreminator(nn.Module):

	"""The class is the basic model for all descriminator
	in out system. Hear we use a torch Seuental layer for
	create a simple nieral network, weach consist of 3 
	layers with activations functions


    METHODS

	__init__(params) - constructure of class. Input value 
	is paramets of GAN. Write this fields in file params.py

	"""


	def __init__(self, params):	

 
		super(Descreminator, self).__init__()

		self.rnn = torch.nn.GRU(1, 1,batch_first = True)
		self.sig = torch.nn.Sigmoid()

	def forward(self, x):

		# x size torch.Size([2, 256, 1])
		
		return self.sig(self.rnn(x)[-1])


class Generator(nn.Module):

	"""The class is the model for generator.Hear we use cicle
	   for creating NN with the same parametrs. 

	   FIELDS
	      parts  - the array of model, what you use in GAN.If 
	      equels 5 it's mean,that GAN Generator consist of 5
	      equels NN.


	    METHODS
		__init__(params) - constructure of class. Input value 
		is paramets of GAN. Write this fields in file params.py

		1.Version make a 3 linear layers with activation function
		2.Version RNN

		forward
		    Function make forward in out net.
		     x - vector for Type music. One element equels 1 and
		     	n - 1 equels 0, where n is the number of type music
		     	in out GAN.
		        For example you can use such record:
		            x = torch.Tensor([[[0,1,0,0,0,0,0,0]], 
                                      [[0,1,0,0,0,0,1,0]]])

	"""

	def __init__(self, params):

		super(Generator, self).__init__()

		self.params = params

		self.parts = []

		self.rnn = torch.nn.GRU(params["numMusicType"],
			                    params["hiddenSizeGener"], 
			                    num_layers = 3,
			                    batch_first=True
			                    )



	def forward(self, x):

		# Make noize
		noize = torch.randn(self.params["butchSize"],
							self.params["lenNoize"],
							self.params["numMusicType"])

		out = self.rnn(torch.Tensor(torch.mul(x,noize)))
		return out[0]        
